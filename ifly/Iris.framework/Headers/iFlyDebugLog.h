//
//  iFlyLogShow.h
//  MSC

//  description: 程序中的log处理类

//  Created by ypzhao on 12-11-22.
//  Copyright (c) 2012年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iFlyDebugLog : NSObject

/**
 * @fn      showLog
 * @brief   打印log
 *
 * @return
 * @param   format          -[in] 要打印的内容
 * @see
 */
+ (void) showLog:(NSString *)format, ...;

/**
 * @fn      writeLog
 * @brief   将log写入文件中
 *
 * 现在还有问题，暂时不写入
 *
 * @return
 * @see
 */
+ (void) writeLog;

+ (void) setShowLog:(BOOL) showLog;
@end
