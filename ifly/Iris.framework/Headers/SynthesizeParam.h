//
//  ReadOptions.h
//  EduMSCDemo
//  合成参数类
//  Created by 钟振东 on 13-6-5.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    CustomPhrase = 1,
    CustomSentence = 2,
    TextWYSWYG  = 4,
    DigitWYSWYG   = 8,
    KeepOrder = 16
}EnumPhraseOption;

typedef enum {
    Default = 0,
    Character  = 1,
    Word = 2,
    Sentence  = 3,
    Passage  = 4,
    Evaluate  = 1000
}EnumSynthType;

typedef enum {
    PlainText   = 1,
    CSSML  = 2
}EnumTextType;

typedef enum {
    XiaoYan = 1,
    XiaoYu = 2,
    Catherine  = 3,
    Henry   = 4,
}Speaker;

/*!
 @class SynthesizeParam
 @brief	合成参数
 */

@interface SynthesizeParam : NSObject

/**
 *	@brief	合成文本类型
            - 1:纯文本 
            - 2:CSSML文本
 */
@property(readwrite) int textType;

/**
 *	@brief	合成类型
            - 1:单字
            - 2:词语
            - 3:句子
            - 4:篇章
 */
@property(readwrite) int synthType;

/**
 *	@brief	合成文本
 */
@property(readwrite,strong) NSString *synthText;

/**
 *	@brief	发音人id
            - 1:XiaoYan(中文女声)
            - 2:XiaoYu(中文男声)
            - 3:Catherine(英文女声)
            - 4:Henry(英文男声)
 */
@property(readwrite) int vid;

/**
 *	@brief	背景音
            - 0:无背景音 
            - 1:梁祝
 */
@property(readwrite) int bgSound;

/**
 *	@brief	合成音速
            - -2:最慢
            - -1:较慢 
            - 0:中等 
            - 1:较快 
            - 2:最快
 */
@property(readwrite) int speed;

/**
 *	@brief	合成解析过程选项（默认12）
            - 1:不用TTS分词,每个字当做一个词
            - 2:不用TTS分句,用换行分句多换行分段
            - 4:文本高保真
            - 8:数字高保真
            - 16:拼音顺序按发音顺序(默认按文本)
            - 32:是否使用英文引擎(不用设置，会根据发音人自动选择)
            - 64:是否实时返回拼音文本(默认合成结束后一起返回所有的拼音)
 */
@property(readwrite) int phraseOption;


@property(readwrite) BOOL isAutoPlay;

/**
 *	@brief	初始化对象
 *
 *	@return	合成参数对象
 */
-(id) init;


//-(BOOL) IsEvaluate;
//-(int) getSynthType;
//-(BOOL) IsEnglishOnly;

@end
