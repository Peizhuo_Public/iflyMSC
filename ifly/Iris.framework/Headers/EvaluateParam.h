//
//  EvaluateContext.h
//  EduMSC
//
//  Created by dijin on 13-6-21.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EvalTypes.h"

/*!
 @header EvaluateParam.h
 @brief	评测参数
 */

@interface EvaluateParam : NSObject
/**
 *	@brief	应用端定义的评测类型名
    - read_chapter          题型 － 英文章节
    - read_sentence         题型 － 英文句子
    - read_word             题型 － 英文章节
    - cn_read_chapter       题型 － 中文章节
    - cn_read_sentence      题型 － 中文句子
    - cn_read_word          题型 － 中文词语
    - cn_read_syllable      题型 － 中文单字
 */
@property (nonatomic,strong) NSString *evalType;

/**
 *
 * @brief 引擎分类 不指定时使用公共引擎
 *      - lab	畅言实验室
 *      - ets	e听说
 */
@property (nonatomic,strong) NSString *engineCategory;

/**
 *	@brief	评测内容
 */
@property (nonatomic,strong) NSString *evalText;

/**
 *	@brief	是否开启朗读跟踪, 只对read_chapter、read_sentence、cn_read_chapter、cn_read_sentence这四种题型有效。 取值如下：
            - true      开启 
            - false     关闭
 */
@property (nonatomic,assign) BOOL autoTrack;

/**
 *	@brief	评测跟踪类型， 取值如下：
            - easy      松模式跟踪(可从当前句末跳到上一句句首，或跳到下下句句首)
            - hard      严模式跟踪(句子不可跳读)
 */
@property (nonatomic,strong) NSString *trackType;
/**
 *	@brief	是否开启端点检测功能, 取值如下:  
            - true      开启端点检测 
            - false     不开端点检测
 */
@property (nonatomic,assign) BOOL vadEnable;

/**
 *	@brief	客户端录音尾部静音最大长度(单位:ms, 取值范围0-30000) 尾部静音长度。如果尾部静音长度超过了此值，则认为用户音频已经结束，此参数仅在打开VAD功能时有效。
 */
@property (nonatomic,assign) int vadSpeechTail;
/**
 *	@brief	客户端录音头部静音最大长度(单位:ms, 取值范围0-30000) 最大音频长度。如果静音长度超过了此值，则认为用户此次无有效音频输入。此参数仅在打开VAD功能时有效
 */
@property (nonatomic,assign) int vadTimeout;

/**
 *	@brief	录音ID, 用来唯一标识此次会话中录音。使用同一个录音ID，后面的音频会覆盖前一个相同ID的音频
 */
@property (nonatomic,strong) NSString  *sndId;

- (id)init;

- (NSString *) getInitParamString;
- (NSData *)   getEvalBuffer;
- (NSString *) getFinalEnt;

@end
