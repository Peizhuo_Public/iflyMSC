//
//  SynthController.h
//
//  合成
//  Created by 钟振东 on 13-6-9.
//  Copyright (c) 2013年 ifly. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SynthesizeDelegate.h"
#import "SynthesizeParam.h"

#import "iflyMSC/IFlySpeechSynthesizerDelegate.h"
#import "iflyMSC/IFlySpeechSynthesizer.h"
#import "iflyMSC/IFlyDataDownloader.h"

/*!
 @header SynthController.h
 @brief	合成服务接口
 */

@interface SynthesizeService : NSObject <IFlySpeechSynthesizerDelegate>

/**
 *	@brief	回调对象
 */
@property (nonatomic,assign) id<SynthesizeDelegate> delegate;/** 设置识别的委托对象 */

@property (nonatomic,strong) SynthesizeParam *synthParams;
@property (nonatomic,assign) BOOL isFirst;

/**
 *	@brief	初始化方法
 *
 *	@param 	delegate 	代理对象
 *	@param 	server_addr 	服务地址
 *
 *	@return	合成对象
 */
- (id)initWithServer:(NSString *)server_addr;

/**
 *	@brief	开始合成
 *
 *	@param 	synthParams 	合成参数
 */
-(void) beginSynthesize:(SynthesizeParam*)synthParams;

/**
 *	@brief	中止合成
 */
-(void) abortSynthetize;

/**
 *	@brief	暂停合成播放，合成依然进行
 */
-(void) pauseSpeaking;

/**
 *	@brief	继续合成播放
 */
-(void) resumeSpeaking;


 
@end
