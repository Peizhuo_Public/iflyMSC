//
//  SpeechUtil.h
//  SpeechUtil
//
//  @author: yuwang@iflytek.com
//  @date: 2014/01/18
//  @version: 1.0.0.0
//

#import <Foundation/Foundation.h>

/*!
 @abstract 语音应用帮助类
 */
@interface SpeechUtil : NSObject

/*!
 @abstract 从合成JSON获取标记评测文本
 @param 合成JSON
 */
+(NSString *)getEvalPapperFromTTSJson: (NSString *)ttsJSON;

@end
