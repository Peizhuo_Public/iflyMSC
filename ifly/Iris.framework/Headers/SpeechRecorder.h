//
//  SpeechRecorder.h
//  EduMSC/Users/apple/zerd/iflytek/SpeechRecorder/Sources/SpeechRecorder/EduMSC/SpeechRecorder.h
//  
//  Created by dijin on 13-7-1.changed by zdzhong on 13-12-9
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

/*!
 @header SpeechReocrder.h
 @abstract 合成、评测、听写服务集成接口
 @brief	SpeechRecorder服务接口
 */

#import <Foundation/Foundation.h>
#import "iflyMSC/IFlySpeechError.h"

#import "SpeechRecorderDelegate.h"

#import "SynthesizeDelegate.h"
#import "RecognizeDelegate.h"
#import "EvaluateDelegate.h"

#import "EvaluateParam.h"
#import "SynthesizeParam.h"

#import "InitOption.h"


@interface SpeechRecorder : NSObject<EvaluateDelegate, RecorgnizeDelegate, SynthesizeDelegate>

/**
 *	@brief	回调对象
 */
@property(unsafe_unretained) id<SpeechRecorderDelegate> SpeechRecorderEvent;

/**
 *	@brief	初始化接口
 *
 *	@param 	initOption 	InitOption对象
 *
 *	@return	SpeechRecorder对象
 */
-(id)initWithOption:(InitOption *)initOption;


#pragma mark -
#pragma mark -合成接口

/*!
 @abstract  开始合成
 */
-(void)beginSynthesize:(SynthesizeParam *)synthParam;

/*!
 @abstract 取消合成
 */
-(void)abortSynthesize;

#pragma mark -
#pragma mark -评测接口

/*!
 @abstract 开始评测
 @param 评测参数 
 @see EvaluateParam
 */
- (void)beginEvaluate:(EvaluateParam *)evalParam;

/*!
 @abstract 完成评测 － 停止录音并提交数据到服务端获取评测结果
 */
-(void)endEvaluate;

/*!
 取消评测
 */
-(void)abortEvaluate;

#pragma mark -
#pragma mark -评测接口
/*!
 @abstract 开始听写
 @param 听写参数字符串
 */
-(void)beginRecognize:(NSString *)recogParam;

/*!
 @abstract 完成听写
 */
-(void)endRecognize;

/*!
 @abstract 取消听写
 */
-(void)abortRecognize;

//-(void)login:(NSString *)userid pwd:(NSString*)pwd param:(NSString *)param;

#pragma mark -
#pragma mark -音频下载播放接口

/**
 *	@brief	加载音频，评测和听写
 *
 *	@param 	audioSrc 	音频源信息
 *	@param 	autoPlay 	加载完成是否自动播放
 */
- (void)loadAudio:(NSString*)audioSrc :(BOOL)autoPlay;

/**
 *  @brief 播放音频
 */
- (void)playAudio;

/**
 *  @brief 区域播放
 *
 *  @param begPos 开始时间 单位：ms
 *  @param endPos 结束时间 单位：ms
 */
- (void)playRange:(uint)begPos :(uint)endPos;

/**
 *  @brief 暂停播放音频
 */
- (void)pauseAudio;

/**
 *  @brief 停止播放音频
 */
- (void)stopAudio;

@end
