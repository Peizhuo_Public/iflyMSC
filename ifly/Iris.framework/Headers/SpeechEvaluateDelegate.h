//
//  SpeechEvaluateDelegate.h
//  EduMSC
//
//  Created by dijin on 13-7-1.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @abstract 评测回调委托声明
 */
@protocol SpeechEvaluateDelegate <NSObject>
/**
 * @fn      onVolumeChanged
 * @brief   音量变化回调
 *
 * @param   volume      -[in] 录音的音量，音量范围1~100
 * @see
 */
- (void) onVolumeChanged: (int)volume;

/**
 * @fn      onBeginOfSpeech
 * @brief   开始识别回调
 *
 * @see
 */
- (void) onBeginOfSpeech;

/**
 * @fn      onEndOfSpeech
 * @brief   停止录音回调
 *
 * @see
 */
- (void) onEndOfSpeech;

/**
 * @fn      onError
 * @brief   识别结束回调
 *
 * @param   errorCode   -[out] 错误类，具体用法见IFlySpeechError
 */
- (void) onError:(IFlySpeechError *) errorCode;

///**
// * @fn      onResults
// * @brief   识别结果回调
// *
// * @param   result      -[out] 识别结果，xml或者json格式，由JsonResult参数决定
// * @see
// */
//- (void) onResults:(NSString *) results;

/**
 * @fn      onResults
 * @brief   识别结果回调
 *
 * @param   resultType   评测结果类型
 * @param result 评测结果
 */
@optional
-(void)onEvalResult:(int)resultType result:(NSString *)result;
/**
 * @fn      onCancal
 * @brief   取消识别回调
 *
 * @see
 */

- (void) onCancel;
@end

