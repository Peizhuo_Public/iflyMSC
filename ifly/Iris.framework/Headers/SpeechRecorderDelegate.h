//
//  SpeechRecorderDelegate.h
//  EduMSC
//
//  Created by dijin on 13-7-2.changed by zdzhong.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @protocol SpeechRecorderDelegate
 @brief	SpeechRecorder代理协议
 */

@protocol SpeechRecorderDelegate <NSObject>

#pragma mark -评测回调
@optional
/**
 *	@brief	评测结果回调
 *  @param dic  属性：
 *	 	- resultType 	识别结果类型，取值(int):
 *        - 3   ERROR               评测结果出错
 *        - 4   SCORE               评测最终结果
 *        - 6   ENDPOINT            语音后端点
 *        - 7   TRACKINFO           朗读跟踪
 *        - 12  SLIGHT_WARNING		轻微警告
 *        - 14  RETELING            背诵转写中间结果
 *
 *	 	- reslt 	识别结果(id)
 *  - 使用示例：    int resultType = [[dic objectForKey:@"resultType"]intValue];
 *              id result = [dic objectForKey:@"result"];
 *
 */
- (void) onEvalResult:(NSDictionary *)dic;

/*!
 *  @brief  录音状态回调
 *  @param  dic 属性:
            - state 录音状态,取值(int):
                - 0: 开始录音  
                - 1: 结束录音
 *  - 使用示例：
 *  int state = [[dic valueForKey:@"state"]intValue];
 */
@optional
- (void) onRecordState:(NSDictionary *)dic;

#pragma mark -听写回调
/*!
 @abstract听写开始，可以开始录音了
 */
@optional
- (void) onIAtReady;

/*!
*   @brief 听写结果回调
*   @param dic 属性:
*          - result 听写结果(NSString)
*   - 使用示例:
*   NSString *result = [dic objectForKey:@"result"];
 */
@optional
- (void) onIAtResult:(NSDictionary *)dic;

/**
 *  @brief	完成时回调
 *  @param  dic 属性:
 *          - audiokey 下载听写音频的key(NSString)
 *   - 使用示例:
 *   NSString *audioKey = [dic objectForKey:@"audioKey"];

 */
@optional
- (void) onIATComplete:(NSDictionary *)dic;

#pragma mark -合成回调
/*!
 @abstract合成开始
 */
@optional
-(void) onSynthBegin;

/**
 *  @brief	合成进度
 *  @param  dic 属性:
 *          - progress 合成进度(float)0-1
 *  - 使用示例:
 *  float progress = [[dic objectForKey:@"progress"]floatValue];
 */
- (void) onSynthProgress:(NSDictionary *)dic;

/*!
 *   @brief 听写结果回调
 *   @param dic 属性:
 *          - result 听写结果json(NSString)
 *   - 使用示例:
 *   NSString *result = [dic objectForKey:@"result"];
 */
@optional
- (void) onSynthResult:(NSDictionary *)dic;

#pragma mark -音频相关回调
@optional
/**
 *	@brief	加载音频完成时回调
 *
 *	@param 	dic 属性:
 *          - audioLength     音频时长    ms
 *          - audioPath       音频路径
 *  - 使用示例:
 *      NSString *audioPath = [dic objectForKey:@"audioPath"];
 *      float audioLength = [[dic objectForKey:@"audioLength"]floatValue];
 *
 */
- (void) onAudioLoaded:(NSDictionary *)dic;

@optional
/**
 *  播放进度
 *
 *  @param dic 属性:
       - energy      声音大小
       - postion     播放位置    ms
 */
- (void) onPlaying:(NSDictionary *)dic;

@optional
/**
 *  播放状态回调
 *
 *  @param dic 属性
 *      - oldState    原状态
 *           - IDLE     空闲状态
 *           - PLAYING     播放状态
 *           - PAUSE       暂停状态
 *      - newState    现状态
 */
- (void) onPlayState:(NSDictionary *)dic;

#pragma mark -发生错误回调
/**
 *	@brief	发生错误时回调
 *  @param dic属性
 *      - errorType:  错误类型 int
 *           - UnknownError	 -1	未知错误
 *           - AppError       1	应用层错误
 *           - MSXError       2	MSC或MSP错误
 *           - EngineError    3	引擎错误
 *      - errorCode:  错误代码 int
 *      - msg:        错误信息 String
 */
- (void) onError:(NSDictionary *)dic;


@end
