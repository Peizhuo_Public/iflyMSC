//
//  InitOption.h
//  EduMSC
//
//  Created by apple  on 13-12-4.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

/*!
 @header InitOption.h
 @abstract SpeechRecorder初始化参数，可设置合成、评测、听写的地址
 @brief	SpeechRecorder参数
 */

#import <Foundation/Foundation.h>
#import "iflyMSC/IFlySetting.h"

@interface InitOption : NSObject

/**
 *  合成地址
 */
@property (nonatomic,strong) NSString *ttsServer;


/**
 *  评测地址
 */
@property (nonatomic,strong) NSString *evalServer;
/**
 *  评测超时时间设置
 */
@property (nonatomic,assign) int evalTimeOut;

/**
 *  听写地址
 */
@property (nonatomic,strong) NSString *iatServer;
/**
 *  听写超时时间设置
 */
@property (nonatomic,assign) int iatTimeOut;

/**
 *  是否加速,暂时不可用
 */
@property (nonatomic,assign) BOOL useAccelerator;

/**
 *  设置日志等级,默认是ALL
 */
@property (nonatomic,assign) LOG_LEVEL lvl;


@end
