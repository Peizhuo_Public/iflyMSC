//
//  RecognizerService.h
//  EduMSC
//
//  Created by dijin on 13-6-29.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iflyMSC/IFlySpeechRecognizer.h"
#import "SpeechRecorgnizeDelegate.h"

@interface RecognizerService : NSObject<IFlySpeechRecognizerDelegate>
{
    IFlySpeechRecognizer            *_iFlySpeechRecognizer;
    NSString *_server;
//    NSString *_AppID;
//    id<SpeechRecorgnizeDelegate> *SpeechRecognizerHandler;
}

-(id)init:(NSString *)server;

@property(unsafe_unretained,nonatomic) id<SpeechRecorgnizeDelegate> SpeechRecognizerHandler;

-(void)Start;
-(void)Cancel;
-(void)Stop;

@end
