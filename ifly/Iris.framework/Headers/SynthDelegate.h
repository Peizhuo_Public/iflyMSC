//
//  TTSDelegate.h
//  TTSProcessor
//
//  Created by 钟振东 on 13-7-1.
//  Copyright (c) 2013年 ifly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SynthesizeParam.h"
#import "iflyMSC/IFlySpeechError.h"

@class SynthDelegate;

@protocol SynthDelegate <NSObject>
//协议声明回调方法

/** 开始合成事件 */
-(void) onSpeakBegin;

/** 语音播放进度 */
- (void) onSpeakProgress:(float)progress;

/** 结束合成的回调
 
 当整个合成结束之后会回调此函数
 
 @param error 错误码
 */
- (void) onCompleted:(IFlySpeechError*) error;

- (void) onJsonCompleted:(NSString*) ttsJson;
@end
