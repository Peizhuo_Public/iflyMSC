//
//  EvaluateService.h
//  SpeechRecorder
//
//  Created by dijin on 13-6-21.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

/*!
 @header EvaluateService.h
 @brief	评测服务接口
 */

#import <Foundation/Foundation.h>
#import "iflyMSC/IFlySpeechEvaluater.h"
#import "iflyMSC/IFlyDataDownloader.h"
#import "iflyMSC/iFlySpeechUser.h"

#import "EvaluateDelegate.h"
#import "EvaluateParam.h"

@interface EvaluateService : NSObject<IFlySpeechEvaluaterDelegate,IFlyDataDownloaderDelegate,IFlySpeechUserDelegate>

/**
 *	@brief	评测代理对象
 */
@property(nonatomic,assign) id<EvaluateDelegate> delegate;

/**
 *	@brief	是否返回json结果，如果为否，则返回xml格式的结果,默认为YES
 */
@property BOOL JsonResult;



/**
 *	@brief	初始化接口
 *
 *	@param 	ctx 	初始化参数
 *	@param 	server_addr 	评测地址
 *
 *	@return	合成对象
 */
-(id)initWithServer:(NSString *)server_addr andTimeout:(int)timeout;

/*!
 @abstract 开始评测
 */
-(void)beginEvaluate:(EvaluateParam *)evalParams;

/*!
 @abstract 停止录音，将数据发往服务端进行评测
 */
-(void)endEvaluate;

/*!
 @abstract 取消评测
 */
-(void)abortEvaluate;

/*!
 @abstract 对结果进行转换，可以指定要处理为json的parser实例，如果未指定则返回xml
 @param result 返回的原始结果
 @param parser 对结果进行处理并转换为json的解析器，如果为nil，则使用默认解析器处理
 */
-(NSString *)GetResult:(NSString *)result parser:(id)parser;

/**
 *	@brief	加载音频，评测和听写
 *
 *	@param 	audioSrc 	音频源信息
 *	@param 	autoPlay 	加载完成是否自动播放
 */
-(void)loadAudio:(NSString*)audioSrc :(BOOL)autoPlay;

@end
