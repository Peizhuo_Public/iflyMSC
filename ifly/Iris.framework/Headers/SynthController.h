//
//  SynthController.h
//
//  合成
//  Created by 钟振东 on 13-6-9.
//  Copyright (c) 2013年 ifly. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SynthDelegate.h"
#import "SynthesizeParam.h"

#import "iflyMSC/IFlySpeechSynthesizerDelegate.h"
#import "iflyMSC/IFlySpeechSynthesizer.h"
#import "iflyMSC/IFlyDataDownloader.h"

#define SYN_TEXT_SHOW @"科大讯飞作为中国最大的智能语音技术提供商，在智能语音技术领域有着长期的研究积累、\
并在中文语音合成、语音识别、口语评测等多项技术上拥有国际领先的成果。科大讯飞是我国唯一以语音技术为产业化方\
向的“国家863计划成果产业化基地”、“国家规划布局内重点软件企业”、“国家火炬计划重点高新技术企业”、\
“国家高技术产业化示范工程”，并被信息产业部确定为中文语音交互技术标准工作组组长单位，\
牵头制定中文语音技术标准。2003年，科大讯飞获迄今中国语音产业唯一的“国家科技进步奖（二等）”，\
2005年获中国信息产业自主创新最高荣誉“信息产业重大技术发明奖”。2006年至2009年，\
连续四届英文语音合成国际大赛（Blizzard Challenge ）荣获第一名。2008年获国际说话人识别评测大赛\
（美国国家标准技术研究院—NIST 2008）桂冠，2009年获得国际语种识别评测大赛（NIST 2009）高难度混淆方言\
测试指标冠军、通用测试指标亚军。"

#define  SYN_SHORT_300 @"打从中共中央政治局常委、国务院总理李克强4个月前的一次突然造访后，内蒙古包头市民高俊平的生活就变得不一样了:不断有记者专门前来对他进行采访、拍照，邻居们总爱跟他打听总理说了些啥，连他4岁的小孙子，也成了远近闻名的小名人。因为李克强造访他家时，他在众多记者的镜头前，光着屁股从衣柜里爬进爬出。这段画面在《新闻联播》播出后，迅速成为网络上点击最热门的视频之一。每当人们看到小屁孩，就会情不自禁地联想起包头市北梁的棚户区。这种变化甚至一直持续到现在。6月14日下午，56岁的高俊平和老伴一起走进了该市刚刚建成的惠民新城小区。按照当地政府的规划，曾以为一辈子都会住在棚户区的高俊平，将很快有机会住进这个崭新的现代居民社区。";
	
#define SYNT @"打从中共中央政治局常委、国务院总理李克强4个月前的一次突然造访后，内蒙古包头市民高俊平的生活就变得不一样了:不断有记者专门前来对他进行采访、拍照，邻居们总爱跟他打听总理说了些啥，连他4岁的小孙子，也成了远近闻名的小名人。因为李克强造访他家时，他在众多记者的镜头前，光着屁股从衣柜里爬进爬出。这段画面在《新闻联播》播出后，迅速成为网络上点击最热门的视频之一。每当人们看到小屁孩，就会情不自禁地联想起包头市北梁的棚户区。这种变化甚至一直持续到现在。6月14日下午，56岁的高俊平和老伴一起走进了该市刚刚建成的惠民新城小区。按照当地政府的规划，曾以为一辈子都会住在棚户区的高俊平，将很快有机会住进这个崭新的现代居民社区。这小区真不赖，比我想象得好太多了。高俊平站在敞亮的样板房里四下里打量，笑得合不拢嘴。此前的几十年，高俊平一家一直住在一间20平方米的小平房里。一个灶台、一个衣柜、一铺炕和一张床，就把屋子塞得满满当当，几乎再找不到下脚的地方。烟熏火燎的煤炉、需要排队等待的旱厕、漏雨的房顶，是他几十年里每天需要面对的生活环境。在这样的环境里，老高跟老伴结婚生子，在这里给儿子娶了媳妇、帮他们照顾孙子，也在这里经历了买断工龄、下岗生活的剧变。最艰难的时候，为了维持生活，年过半百的老伴成了四处给人做家务的小时工，而曾经是铸造工人的高俊平，也只能去餐馆帮人打打零工，摘棵葱、剥个蒜。在占地13平方公里的包头市北梁棚户区，像高俊平这样的城市贫民共有12.4万人。根据当地政府统计的数据，他们的人均住房面积不足15平方米，人均月收入不足500元。李克强就曾经来到北梁棚户区考察调研并指出:棚户区改造是改善民生、促进发展的硬任务。今年2月3日，他再次来到包头市考察，并再次来到这片规模罕见的集中连片棚户区查看实情。沿着一条泥泞、狭窄的巷道，李克强深一脚浅一脚随机走进了高俊平家里。直到4个月后的今天，高俊平仍能清晰地回忆起那天的情景。当时，他正在床头切肉，准备烩点酸菜当作小年夜的晚饭，没想到院门突然被人推开，一群人呼呼啦啦地走进了院子。老高当时有点懵了。直到有人告诉他，这是李克强总理，他才回过神来，并且紧紧地攥住了李克强的手。我当时太激动了。高俊平说。他记得，李克强一点儿也没有大官的架子。他穿着一条和自己一样的黑裤子，跟自己一起随意并排坐在床沿上，很自然地拉着家常，询问自己房子漏不漏雨、上厕所方不方便";

@interface SynthController : NSObject <IFlySpeechSynthesizerDelegate,IFlyDataDownloaderDelegate>
{
    IFlySpeechSynthesizer       *_iFlySpeechSynthesizer;

    NSMutableArray         *_pyBufferArray;         //拼音Buffer缓存

    bool  PYbufferOver;                             //拼音Buffer处理进度
    
    IFlySpeechError *ttsError;
    
    id<SynthDelegate>   __unsafe_unretained _delegate;
    
    IFlyDataDownloader *_downloader;
    
    NSMutableData *_AudioData;
    
    SynthesizeParam *SynParam;
    
    BOOL IsRunning;
}

@property (assign,nonatomic) BOOL IsRunning;
@property (unsafe_unretained) id<SynthDelegate> delegate;/** 设置识别的委托对象 */

/**
 * @fn      init
 * @brief   初始化对象，创建合成句柄 
 * @see
 */
- (id)initWithDelegate:(id<SynthDelegate>)delegate andServer:(NSString *)server_addr;

/**
 * @fn      SynthTTS
 * @brief   调用合成
 *
 * &&&& 传入合成的参数
 *
 * @param   synthParams    -[in] ReadOptions类型的合成参数 
 * @see
 */
-(void) asyncSynthesize:(SynthesizeParam*)synthParams;

- (void)BeginSynth:(NSString*)ttsReadOption;//开始合成

-(void) abortSynthetize;//终止合成

-(void) pauseSpeaking;//暂停播放暂停播放之后，合成不会暂停，仍会继续，如果发生错误则会回调错误`onCompleted

-(void) resumeSpeaking;/** 恢复播放 */

 
@end
