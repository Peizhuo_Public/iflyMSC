//
//  TTSDelegate.h
//  TTSProcessor
//
//  Created by 钟振东 on 13-7-1.
//  Copyright (c) 2013年 ifly. All rights reserved.
//

/*!
 @protocol SynthDelegate
 @abstract SynthController代理协议
 @brief	SynthController代理协议
 */

#import <Foundation/Foundation.h>
#import "iflyMSC/IFlySpeechError.h"

@protocol SynthesizeDelegate <NSObject>

/**
 *	@brief	开始合成事件
 */
-(void) onSpeakBegin;


/**
 *	@brief	语音播放进度
 *
 *	@param 	progress 	合成进度
 */
- (void) onSpeakProgress:(float)progress;


/**
 *	@brief 当整个合成结束之后会回调此函数
 *  @param error 错误码
 */
- (void) onCompleted:(IFlySpeechError*) error;


/**
 *	@brief	Json结果回调此函数
 *  @param  ttsJson Json结果
 */
- (void) onJsonCompleted:(NSString*) ttsJson;

/**
 *  音频下载完成回调
 *
 *  @param result  音频文件路劲
 *  @param errCode 错误代码
 */
- (void) onSynzDownloadEnd:(NSString *)result errCode:(int)errCode;


@end
