//
//  EvalTypes.h
//  EduMSCDev
//
//  Created by apple  on 13-12-13.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#ifndef EduMSCDev_EvalTypes_h
#define EduMSCDev_EvalTypes_h

/*!
 @header EvalTypes.h
 @abstract 评测操作常量定义
 @author dijin
 */
#import <Foundation/Foundation.h>
/*!
 @abstract 评测类型 － 篇章
 */

//--------------------------------------------------------------------------------
//                                  评测题型定义
//--------------------------------------------------------------------------------
// 英文题型
#define EVALTYPE_READ_CHAPTER @"read_chapter"               // 英文篇章朗读
#define EVALTYPE_READ_SENTENCE  @"read_sentence"            // 英文句子朗读
#define EVALTYPE_READ_WORD @"read_word"                     // 英文单词朗读
#define EVALTYPE_TOPIC  @"topic"                            // 英文话题表述
#define EVALTYPE_SIMPLE_EXPRESSUION @"simple_expression"    // 英文情景反应
#define EVALTYPE_READ_CHOISE @"read_choice"                 // 英文单项选择题
#define EVALTYPE_RETELLING @"retelling"                     // 英文背诵转写
#define EVALTYPE_SENTENCE_IMITATION @"sentence_imitation"   // 英文句子模仿朗读

// 中文题型
#define EVALTYPE_CN_READ_CHAPTER  @"cn_read_chapter"        // 中文篇章朗读
#define EVALTYPE_CN_READ_SENTENCE  @"cn_read_sentence"      // 中文句子朗读
#define EVALTYPE_CN_READ_WORD @"cn_read_word"               // 中文词语朗读
#define EVALTYPE_READ_SYLLABLE  @"read_syllable"            // 中文单字朗读

//--------------------------------------------------------------------------------
//                                  引擎类别定义
//--------------------------------------------------------------------------------
#define EVAL_ENGINETYPE_EN @"en"            // 英文学习引擎
#define EVAL_ENGINETYPE_CN @"cn"            // 中文学习引擎
#define EVAL_ENGINETYPE_CHECK @"endtt"      // 英文检错引擎
#define EVAL_ENGINETYPE_RETELLING @"enrtl"  // 英文背诵转写引擎

//--------------------------------------------------------------------------------
//                                朗读跟踪模式定义
//--------------------------------------------------------------------------------
#define TRACKTYPE_EASY @"easy" // 松模式
#define TRACKTYPE_HARD @"hard" // 严模式

//评测结果类型
typedef enum{
    //错误
    Error = 3,
    //评测成功
    Score = 4,
    //端点
    EndPoint = 6,
    //跟踪信息
    TrackInfo = 7,
    //轻微警告
    Light_Warning = 12,
    //背诵转写中间结果
    Reteling = 14
}EvalReusltType;


static const  int ISE_RESULT_PHONE = 1;     //发音错
static const  int ISE_RESULT_TONE = 2;     //调型错
static const  int ISE_RESULT_LONG_STOP = 4;   //前面停顿过长
static const  int ISE_RESULT_SHORT_STOP  = 8;  //前面停顿过短

// dp_message：单元状态 <0: 正常，16：漏读，32：增读，64：回读， 128:替换，  32768：错读>
static const  int ISE_RESULT_NO_ERR  = 0;
static const  int ISE_RESULT_MISS  = 16;
static const  int ISE_RESULT_INSERT  = 32;
static const  int ISE_RESULT_BACK  = 64;
static const  int ISE_RESULT_ERR  = 32768;


/*
 SeeCNErrorCodes
 */

/**
 *  声母错
 */
static const int ErrorVowel = 1;

/**
 * 韵母错
 */
static const int ErrorConsonant = 2;

/**
 * 声调错
 */
static const int ErrorTone = 4;

/**
 *前面停顿过长
 */
static const int ErrorLongStop = 8;

/**
 *前面停顿过短
 */
static const int ErrorShortStop = 16;

/**
 * 漏读
 */
static const int ErrorMiss = 32;


#endif
