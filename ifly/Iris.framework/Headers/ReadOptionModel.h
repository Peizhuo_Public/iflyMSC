//
//  ReadOptionModel.h
//  SyntheDemo
//
//  Created by 钟振东  on 13-11-14.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import "JSONKit.h"
#import "SynthesizeParam.h"

@interface ReadOptionModel : NSObject

@property(readwrite) int TextType;
@property(readwrite) int SynthType;
@property(readwrite) int Vid;
@property(readwrite) int BgSound;
@property(readwrite) int Speed;
@property(readwrite) int PhraseOption;
@property(strong, nonatomic) NSString *UserField;

- (NSString *)jsonStringWithURLCoding:(SynthesizeParam *)SyntheParam;

@end
