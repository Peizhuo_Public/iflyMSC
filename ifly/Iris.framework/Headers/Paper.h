//
//  Paper.h
//  EduMSC
//
//  Created by dijin on 13-6-21.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 试卷对象
 1. 加载试卷内容
 2. 验证试卷的有效性 － 试卷类型必须和内容中头部定义保持一致
 3. 调整试卷内容 － 例如read_word的内容没有头部［word］，则自动添加
 */
@interface Paper : NSObject
{
    NSString *_paperType;
    NSString *_paperFilePath;
    NSString *_paperContent;
    NSString *_paperDesc;
}

@property (nonatomic,strong) NSString *Content;

@property (nonatomic,strong) NSString *PaperType;

@property (nonatomic,strong) NSString *PaperDescription;

@end
