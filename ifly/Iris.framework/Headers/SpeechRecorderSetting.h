//
//  SpeechRecorderSetting.h
//  EduMSC
//
//  Created by zdzhong  on 13-11-29.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

/*!
 @header SpeechRecorderSetting.h
 @abstract SpeechRecorder设置类
 @brief	SpeechRecorder设置类
 */

#import <Foundation/Foundation.h>

@interface SpeechRecorderSetting : NSObject

/**
 *  类方法，获取版本号
 *
 *  @return 版本号
 */
+(NSString *)getVersion;



@end
