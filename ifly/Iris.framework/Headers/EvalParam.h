//
//  EvalParam.h
//  EduMSC
//
//  Created by dijin on 13-6-9.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @header EvalParam.h
 @abstract 评测参数封装
 */
@interface EvalParam : NSObject
{
    NSString *_engineCategory;
    
    NSString *_evalType;
    
    BOOL _isAutoTrack;
    
    NSString *_trackType;
    
    int _silenceTime;
    
    BOOL _vadEnable;
    
    NSMutableDictionary *_paramsDict;
    
    NSString *_appId;
    
    NSString *_pwd;
    
    int _timeout;
    
    NSString *_engineType;
    
    NSString *_subType;
    
    struct Struct_AutoTrack *_autoTrackSetting;
  }

/*!
 @property 请求类型
 */
@property (nonatomic,strong) NSString *subType;
/*!
 @abstract 引擎类型，如en，cn ，具体值见：EvalType.h中
 */
@property (nonatomic,strong) NSString *enginType;
@property int timeout;
@property (nonatomic,strong) NSString *appId;

@property (nonatomic,strong) NSMutableDictionary *paramsDict;

/*!
 @property 引擎类型
 */
@property (nonatomic,strong) NSString *engineCategory;

/*!
 @property 评测类型
 */
@property (nonatomic,strong) NSString *evalType;

/*!
 @property 是否自动跟踪
 */
@property (nonatomic) BOOL isAutoTrack;

/*!
 @property 自动跟踪类型 － hard(严模式)或者easy(松模式)
 */
@property (nonatomic,strong) NSString *trackType;

@property BOOL vadEnable;

/*!
 @abstract 获取参数字符串
 */
-(NSString *)GetParamString;

-(void)AddParam:(NSString *)paraName paraValue:(NSString *)paraValue;
@end
