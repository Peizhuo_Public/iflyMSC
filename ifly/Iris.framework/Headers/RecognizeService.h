//
//  RecognizerService.h
//  EduMSC
//
//  Created by dijin on 13-6-29.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

/*!
 @header RecognizerService.h
 @brief	听写服务接口
 */

#import <Foundation/Foundation.h>
#import "iflyMSC/IFlySpeechRecognizer.h"
#import "RecognizeDelegate.h"

@interface RecognizeService : NSObject<IFlySpeechRecognizerDelegate,NSURLConnectionDelegate>

/**
 *	@brief	回调对象
 */
@property(assign,nonatomic) id<RecorgnizeDelegate> delegate;


/**
 *	@brief	初始化接口
 *
 *	@param 	server 	听写地址
 *
 *	@return	听写对象
 */
-(id)initWithServer:(NSString *)server_addr andTimeout:(int)timeout;

/**
 *	@brief	开始听写
 */
-(void)beginRecognize;

/**
 *	@brief	取消听写
 */
-(void)abortRecognize;

/**
 *	@brief	中止听写
 */
-(void)stop;

/**
 *	@brief	加载音频，评测和听写
 *
 *	@param 	audioSrc 	音频源信息
 *	@param 	autoPlay 	加载完成是否自动播放
 */
-(void)loadAudio:(NSString*)audioSrc :(BOOL)autoPlay;

@end
