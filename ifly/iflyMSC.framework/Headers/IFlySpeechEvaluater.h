//
//  iFlySpeechEvaluater.h
//  msc
//
//  Created by admin on 13-6-19.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IFlySpeechEvaluaterDelegate.h"

@class IFlyISERecognizer;

/** 语音评测类 */
@interface IFlySpeechEvaluater : NSObject<IFlySpeechEvaluaterDelegate>
{
    IFlyISERecognizer *_mscer;
    NSData            *_buffer;
    
    id<IFlySpeechEvaluaterDelegate> _delegate;
}

/** 设置委托对象 */
@property(assign) id<IFlySpeechEvaluaterDelegate> delegate ;

///---------------------------------------------------------------------------------------
/// @name 创建对象
///---------------------------------------------------------------------------------------

/** 创建识别对象的单类
 
 此方法的使用示例如下：
        NSString *initString = [[NSString alloc] initWithFormat:@"appid=%@,timeout=%@",APPID,TIMEOUT];
        _iFlySpeechEvaluater = [IFlySpeechEvaluater createEvaluater: initString delegate:self];
        [initString release]
 
 @param params  初始化时的参数，
 @param delegate 委托对象
 */
//+ (id) createEvaluater:(NSString *)params delegate:(id<IFlySpeechEvaluaterDelegate>) delegate;


/** 返回识别对象的单例
 */
+ (id) sharedInstance;

/**
 销毁识别对象。
 */
- (BOOL) destroy;

/** 获取识别对象
 
 @return 返回创建的识别对象
 */
//+ (IFlySpeechEvaluater *) getEvaluater;

///---------------------------------------------------------------------------------------
/// @name 操作
///---------------------------------------------------------------------------------------

/** 设置识别引擎的参数
 
 识别的引擎参数(key)取值如下:
 
 1.domain:应用的领域;取值为iat、at,search,video,poi,music,asr;iat,普通文本转写; search,热词搜索;video,视频音乐搜索;asr: 命令词识别;
 2.vad_bos:前端点检测;静音超时时间,即用户多长时间不说话则当做超 时处理,单位:ms,engine 指定 iat 识别默认值为 5000,其他 情况默认值为 4000,范围 0-10000。
 3.vad_eos:后断点检测;后端点静音检测时间,即用户停止说话多长时间 内即认为不再输入,自动停止录音,单位:ms,sms 识别默认 值为 1800,其他默认值为 700,范围 0-10000。
 4.sample_rate:采样率,目前支持的采样率设置有 16000 和 8000。
 5.asr_ptt:否返回无标点符号文本;默认为 1,当设置为 0 时,将返回无标点符号文本。
 6.asr_sch:是否需要进行语义处理,默认为 0,即不进行语义 识别,对于需要使用语义的应用,需要将 asr_sch 设为 1,并且 设置 plain_result 参数为 1,由外部对结果进行解析。
 7.plain_result:回结果是否在内部进行 json 解析,默认值为 0,即进行解析,返回外部的内容为解析后文本。对于语义等业 务,由于服务端返回内容为 xml 或其他格式,需要应用程序自 行处理,这时候需要设置 plain_result 为 1。
 8.grammarID:识别的语法 id,只针对 domain 设置为”asr”的应用。
 9.params:扩展参数,对于一些特殊的参数可在此设置。
 
 @param key 识别引擎参数
 @param value 参数对应的取值
 
 @return 设置的参数和取值正确返回YES,失败返回NO
 */
-(BOOL) setParameter:(NSString *) value forKey:(NSString*)key;

/** 开始评测
 
 同时只能进行一路会话,这次会话没有结束不能进行下一路会话，否则会报错

 @param data 评测的数据
 @param params 评测的参数
 */
- (void) startListening:(NSData *) data params:(NSString *) params;

/** 停止录音
 
 调用此函数会停止录音，并开始进行语音识别
 */
- (void) stopListening;

/** 取消本次会话 */
- (void) cancel;

/** 获取评测信息
 
 @param key 参数的名称
 @return 参数的值
 */
- (NSString *) getMessage:(NSString *) key;

@end
