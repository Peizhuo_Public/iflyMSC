//
//  iFlySpeechEvaluaterDelegate.h
//  msc
//
//  Created by admin on 13-6-19.
//  Copyright (c) 2013年 iflytek. All rights reserved.
//

#import "IFlySpeechRecognizerDelegate.h"

@class IFlySpeechError;

@protocol IFlySpeechEvaluaterDelegate <NSObject>

/** 音量和数据回调
 
 @param volume 音量
 @param buffer 音频数据
 */
- (void) onVolumeChanged:(int)volume buffer:(NSData *) buffer;


/** 开始录音回调
 
 当调用了`startListening`函数之后，如果没有发生错误则会回调此函数。如果发生错误则回调onError:函数
 */
- (void) onBeginOfSpeech;

/** 停止录音回调
 
 当调用了`stopListening`函数或者引擎内部自动检测到断点，如果没有发生错误则回调此函数。如果发生错误则回调onError:函数
 */
- (void) onEndOfSpeech;

/** 正在取消
 */
- (void) onCancel;

/** 识别结果回调
 
 在进行语音识别过程中的任何时刻都有可能回调此函数，你可以根据errorCode进行相应的处理，当errorCode没有错误时，表示此次会话正常结束，否则，表示此次会话有错误发生。特别的当调用`cancel`函数时，引擎不会自动结束，需要等到回调此函数，才表示此次会话结束。在没有回调此函数之前如果重新调用了`startListenging`函数则会报错误。
 
 @param errorCode 错误描述类，
 */
- (void) onError:(IFlySpeechError *) errorCode;

/** 识别结果回调
 
 在识别过程中可能会多次回调此函数，你最好不要在此回调函数中进行界面的更改等操作，只需要将回调的结果保存起来。
 
 使用results的示例如下：
 
         NSMutableString *result = [[NSMutableString alloc] init];
         NSDictionary *dic = [results objectAtIndex:0];
         for (NSString *key in dic) {
             [result appendFormat:@"%@",key];
         }
 
 @param   result      -[out] 识别结果，NSArray的第一个元素为NSDictionary，NSDictionary的key为识别结果，value为置信度。 const char *
 */
- (void) onResults:(NSArray *) results;

//#ifdef _EDUCATION_
- (void) onResultByIse:(NSData *)para;
//#endif
@end
