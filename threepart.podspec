Pod::Spec.new do |s|
  s.name         = "iflyMSC"
  s.version      = "1.2.1"
  s.homepage     = "https://gitlab.com/Peizhuo_Public"
  s.author       = { "iflyMSC" => "PEP" }
  s.summary      = "科大讯飞语音识别SDK"

  s.platform     =  :ios, "7.0"
  s.source       = { :git => "https://gitlab.com/Peizhuo_Public/iflyMSC.git" }

  s.subspec "ifly" do |ifly|
    ifly.subspec "ise" do |ise|
    ise.source_files  = "ifly/ise/*.{h,m}"
    end

    ifly.subspec "results" do |results|
    results.source_files  = "ifly/ise/results/*.{h,m}"
    end

    ifly.subspec "pcm" do |pcm|
    pcm.source_files  = "ifly/pcm/*.{h,m}"
    end

    ifly.vendored_frameworks = "ifly/iflyMSC.framework", "ifly/Iris.framework"

 end

  s.frameworks   =  "CoreLocation","CoreTelephony","AddressBook","AudioToolbox","AVFoundation","SystemConfiguration","Contacts","QuartzCore","CoreGraphics"
  s.requires_arc = true

end
